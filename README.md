## 简介
Spring Boot 项目图片上传样例，附带图片剪切功能

Spring Boot 版本 1.5

截图采用：cropper.js

### 注意
在`application.properties`中设置上传路径，名称是`web.upload.path`设置成图片上传的具体地址

### 步骤
1. 启动项目，在AppRun中启动**main方法**。
2. 访问[localhost:8080](localhost:8080)，打开页面，上传文件，等待上传完成。
3. 进行图片截图处理，可以看到截图的相应坐标，文件名称，提交，等待系统截图。
4. 最后显示截图后的效果。

### 图片未上传之前显示预览效果
1. 启动项目
2. 访问[localhost:8080/previewImg](localhost:8080/previewImg)
3. ie下由于浏览器安全策略问题，可以使用滤镜。